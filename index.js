const Telegraf = require('telegraf');

function trimCommand(input) {
    return input.replace(/\/.+?\s/g, '');
}

function setCharAt(str,index,chr) {
	if(index > str.length-1) return str;
	return str.substr(0,index) + chr + str.substr(index+1);
}

function allCaps(input) {
    return input.toUpperCase();
}

function randomCaps(input) {
    var length = input.length;
    for(var i = 0 ; i < length ; i++) {
        if(Math.round(Math.random())) {
            input = setCharAt(input, i, input.charAt(i).toUpperCase());
        }
    }
    return input;
}

function gappedText(input) {
    var length = input.length;
    var output = '';
    for(var i = 0 ; i < length ; i++) {
        output = output.concat(input[i]);
        if(i != length - 1) {
            output = output.concat(' ');
        }
    }
    return output;
}

function reverseString(input) {
    var length = input.length;
    var output = '';
    for(var i = length - 1 ; !(i < 0) ; i--) {
        output = output.concat(input[i]);
    }
    return output;
}

function squaredText(input) {
    var length = input.length;
    var output = '';
    output = output.concat(gappedText(input));
    output = output.concat('\n');
    for(var i = 1 ; i < length - 1 ; i++) {
        output = output.concat(input[i]);
        for(var j = 0 ; j < length - 2 ; j++) {
            output = output.concat('  ');
        }
        output = output.concat(' ');
        output = output.concat(input[length - i - 1]);
        output = output.concat('\n');
    }
    output = output.concat(reverseString(gappedText(input)));
    output = output.concat('\n');
    return output;
}

const bot = new Telegraf(process.env.BOT_TOKEN);
bot.start((ctx) => ctx.reply('Yo'));
bot.help((ctx) => ctx.reply('No u'));
bot.command('caps', (ctx) => {
    ctx.reply(allCaps(trimCommand(ctx.message.text)));
});
bot.command('rcaps', (ctx) => {
    ctx.reply(randomCaps(trimCommand(ctx.message.text)));
});
bot.command('square', (ctx) => {
    var response = '```\n';
    response = response.concat(squaredText(trimCommand(ctx.message.text))).concat('```');
    ctx.replyWithMarkdown(response);
});
bot.command('gap', (ctx) => {
    ctx.reply(gappedText(trimCommand(ctx.message.text)));
});
bot.command('rev', (ctx) => {
    ctx.reply(reverseString(trimCommand(ctx.message.text)));
});
bot.catch((err) => {
    console.log(err);
})
bot.startPolling();
